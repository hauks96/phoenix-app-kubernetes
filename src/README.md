# App configuration

How to configure your application for kubernetes. Note that this project already has this set up, so this is purely for your information.

## Install and create app

If you don't already have an application and are starting from scratch, this is how you can set it up.

_From within the src/ directory_

```bash
# Get rebar
mix local.rebar --force
# Get hex
mix local.hex --force
# Install hex and phx_new
mix archive.install hex phx_new
# Create new phoenix project in current directory called 'demo'
mix phx.new . --app=demo
```

## Configuring the app for kubernetes

In this section we go over the steps needed to make our application work with kubernetes. The key elements to this setup are the use of the [libcluster](https://hexdocs.pm/libcluster/Cluster.Strategy.Kubernetes.DNS.html) elixir package together with a [headless service](https://kubernetes.io/docs/concepts/services-networking/service/#headless-services) configured in our kubernetes resources.

**Note:**\
You can search for `# ADDED -` (ctrl+shift+f in vscode) to see all the changes and additions made.

## Generate default release files

Phoenix and elixir provide some awesome generation tools to get started with releases. What we need
right now is the following.

1. Binaries to start / stop / migrate our application through terminal command
2. A dockerfile to create a production image for our application
3. A script that allows us to have node discovery in releases

The bash commands below generate defaults for all of the above mentioned requirements. The only generated content we have to modify is no. 3, or more specifically `env.sh.eex` which we will dive into shortly.

_From within the src/ directory_

```bash
# 1. Release overlay binaries (start/stop/migrate etc)
mix phx.gen.release
# 2. Production dockerfile
mix phx.gen.release --docker
# 3. Release env scripts
mix release.init
```

You should now have a Dockerfile which I have renamed to Dockerfile.prod in this project and a /rel folder, giving you the following directory structure:

```
src/
  ...
  Dockerfile
  rel/
    overlays/bin/
      migrate
      migrate.bat
      server
      server.bat
    env.bat.eex
    env.sh.eex
    remote.vm.args.eex
    vm.args.eex
```

## Update the env.sh.eex release script

In the script [env.sh.eex](rel/env.sh.eex) we must modify the RELEASE_NODE environment variable the script exports. This is because our nodes are running in kuberenetes pods which have dynamically assigned IP addresses which are only known internally.

This script is executed as part of the startup process. With it we can determine the IP address of our node and it's DNS so that it becomes discoverable in the application's node cluster. You can read more on kubernetes pod DNS resolution [here](https://kubernetes.io/docs/concepts/services-networking/dns-pod-service/#pods).

_Add this content to rel/env.sh.eex_

```bash
# A kubernetes pod's A record uses the pod's IP with dots replaced with dashes
export POD_A_RECORD=$(echo $K8S_POD_IP | sed 's/\./-/g')
# Mark our release as 'distributed' by setting value to 'name'
export RELEASE_DISTRIBUTION=name
# This is the kubernetes cluster's internal DNS for this pod
export RELEASE_NODE=<%= @release.name %>@${POD_A_RECORD}.${K8S_NAMESPACE}.pod.cluster.local
```

At this point you might be wondering where the environment variable `$K8S_POD_IP` and `$K8S_NAMESPACE` are coming from. These variables are set in the _deployment.yml_ resource file for our application (see [phoenix-app helm chart](../helm/phoenix-app/templates/deployment.yaml)).

## Update runtime.exs

In [config/runtime.exs](config/runtime.exs) we configure the environment variables required at runtime. In general, this file should ensure you only ever need one image for all your production builds. Any configuration that is specific to a production environment such as the database URL can be set with environment variables.

If you are running on an old version of elixir, runtime.exs might not yet be available to you as this is a quite recent addition (elixir v1.11). In that case you must update your elixir version to continue. If you do not have a runtime.exs file within your config directory, simply create it.

I highly recommend opening the [runtime.exs](config/runtime.exs) file for this step and taking a look at the changes to make sure you are getting it right.

First of all, we want to ensure that the environment variables needed for our env.sh.eex script are available at runtime and give a meaninful error message if that isn't the case.

Further we modify the app config with a configurable host and port and remove any reference to https configuration because ssl is terminated at the cluster level through ingresses, not at the application level.

## Update the network topology for production

Libcluster provides network topologies for kubernetes. The one we are going to use is `Elixir.Cluster.Strategy.Kubernetes`. First of all we need to add libcluster as a dependency in [mix.exs](mix.exs).

_Within mix.exs `deps` function_

```elixir
defp deps do
    [
      {:phoenix, "~> 1.7.7"},
      ...
      {:libcluster, "~> 3.3"}
    ]
  end
```

Now we need to add this cluster strategy to our application. I highly recommend opening the [application.ex](lib/demo/application.ex) file for this step and taking a look at the changes to make sure you are doing this correctly.

First we create a method which returns the kubernets topology configuration.
This is all documented in the [libcluster documentation](https://hexdocs.pm/libcluster/3.3.3/Cluster.Strategy.Kubernetes.html),
so take a look there if you need the details.

```elixir
@spec kubernetes_topology :: list
def kubernetes_topology do
  namespace = Application.get_env(:kubernetes, :namespace)
  selector = Application.get_env(:kubernetes, :selector)

  [
    k8s: [
      strategy: Elixir.Cluster.Strategy.Kubernetes,
      config: [
        mode: :dns,
        kubernetes_node_basename: "demo",
        kubernetes_selector: selector,
        kubernetes_namespace: namespace,
        polling_interval: 10_000
      ]
    ]
  ]
end
```

Now we define a helper function to append the topology to our `children` variable.

_At the bottom of the Application module_

```elixir
defp append_if(list, condition, item) do
  if condition, do: list ++ [item], else: list
end
```

_At the beginning of the `start` function_

```elixir
kubernetes_enabled = Application.get_env(:kubernetes, :enabled, false)
```

Finally, we conditionally append our topology to the `children` variable in the `start` function.

```elixir
children =
  [
    ...
  ]
  |> append_if(
    kubernetes_enabled,
    {Cluster.Supervisor, [kubernetes_topology(), [name: Demo.ClusterSupervisor]]}
  )
```

And that's it! The application is now ready for kubernetes integration.

# Phoenix docs

## Run server - using docker-compose

- From the **project root directory** run `docker-compose up`
- Visit `http://localhost:4000`

## Run server - the normal way

If you ran the compose approach previously you can do this

0. Go into /src in terminal
1. sudo chown -R $USER .
2. docker start phoenix-database
3. mix phx.server

You didn't run the compose approach previously

- Create a database matching the credentials in config/dev.exs
- Install dependencies with `mix deps.get`
- Create and migrate your database with `mix ecto.setup`
- Start Phoenix endpoint with `mix phx.server` or inside IEx with `iex -S mix phx.server`

Now you can visit [`localhost:4000`](http://localhost:4000) from your browser.

Ready to run in production? Please [check our deployment guides](https://hexdocs.pm/phoenix/deployment.html).

## Learn more

- Official website: https://www.phoenixframework.org/
- Guides: https://hexdocs.pm/phoenix/overview.html
- Docs: https://hexdocs.pm/phoenix
- Forum: https://elixirforum.com/c/phoenix-forum
- Source: https://github.com/phoenixframework/phoenix
