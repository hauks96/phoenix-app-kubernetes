apiVersion: apps/v1
kind: Deployment
metadata:
  name: {{ include "common.name" . }}
  labels:
    {{- include "common.labels" . | nindent 4 }}
spec:
  replicas: {{ .Values.deployment.podReplicas }}
  strategy:
    type: RollingUpdate
    rollingUpdate:
      maxSurge: {{ .Values.deployment.rollingUpdateMaxSurge }}
      maxUnavailable: {{ .Values.deployment.rollingUpdateMaxUnavailable }}
  selector:
    matchLabels:
      release: {{ include "common.name" . }}
  template:
    metadata:
      annotations:
        {{- if eq .Values.image.pullPolicy "Always" }}
        # Force redeployment - In case container has same tag but is actually updated
        rollme: {{ randAlphaNum 5 | quote }}
        {{- end }}
        checksum/env: {{ include (print $.Template.BasePath "/env-secrets.yaml") . | sha256sum }}
      labels:
        {{- include "common.labels" . | nindent 8 }}
    spec:
      serviceAccountName: {{ include "common.name" . }}
      containers:
        - name: {{ .Values.container.name }}
          image: "{{ .Values.image.repository }}:{{ .Values.image.tag }}"
          imagePullPolicy: {{ .Values.image.pullPolicy }}
          ports:
            - containerPort: {{ .Values.container.internalPort }}
              protocol: TCP
          resources:
            requests:
              memory: {{ .Values.container.resourceRequests.memory}} 
              cpu: {{ .Values.container.resourceRequests.cpu}}
            limits:
              memory: {{ .Values.container.resourceLimits.memory}} 
              cpu: {{ .Values.container.resourceLimits.cpu}}
          env:
            - name: K8S_POD_IP
              valueFrom:
                fieldRef:
                  fieldPath: status.podIP
            - name: K8S_NAMESPACE
              valueFrom:
                fieldRef:
                  fieldPath: metadata.namespace
            - name: K8S_SELECTOR
              value: {{ include "phoenix.selector" . }}
            - name: PHX_PORT
              value: {{ .Values.container.internalPort | quote }}
          envFrom:
            - secretRef:
                name: {{ include "common.name" . }}
          livenessProbe:
            httpGet:
              path: {{ .Values.container.livenessProbePath }}
              port: {{ .Values.container.internalPort }}
            initialDelaySeconds: 15
            periodSeconds: 120
            failureThreshold: 3
            timeoutSeconds: 5
          readinessProbe:
            httpGet:
              path: {{ .Values.container.readinessProbePath }}
              port: {{ .Values.container.internalPort }}
            initialDelaySeconds: 10
            timeoutSeconds: 2
            failureThreshold: 3
            periodSeconds: 60
      {{- if .Values.image.private }}
      imagePullSecrets:
        - name: {{ (printf "image-pull-secret-%s" (include "common.name" .)) | trunc 63 | trimSuffix "-" }}
      {{- end }}

