CLUSTER_NAME="phoenix-cluster"
IMAGE="phoenix-demo"
TAG="1.0.0"
NAMESPACE="demo"
RELEASE_DB="demo-db"
RELEASE_APP="demo-app"

DB_NAME="phoenix"
DB_USER="phoenix_user"
DB_PASSWORD="phoenix_password"
DB_HOST="phoenix-db-service"
DB_PORT="7432"

set -euo pipefail

# Create a cluster
kind create cluster --config ./kubernetes/kind.config.yaml --name $CLUSTER_NAME
kubectl config set-context kind-$CLUSTER_NAME

# Install traefik on cluster
helm repo add traefik https://helm.traefik.io/traefik
helm repo update
helm install traefik traefik/traefik --version "24.0.0" -n traefik --create-namespace -f ./kubernetes/traefik.values.yaml

# Build docker container
docker build -t $IMAGE:$TAG -f "src/Dockerfile.prod" "./src"
kind load docker-image $IMAGE:$TAG --name $CLUSTER_NAME

# Install database
helm upgrade $RELEASE_DB ./helm/database --install \
--set environment.POSTGRES_DB="postgres" \
--set environment.POSTGRES_USER="dev" \
--set environment.POSTGRES_PASSWORD="dev" \
--set environment.DATABASE_NAME="$DB_NAME" \
--set environment.DATABASE_USER="$DB_USER" \
--set environment.DATABASE_PASSWORD="$DB_PASSWORD" \
--set serviceName="$DB_HOST" \
--set container.exposedPort="$DB_PORT" \
-n $NAMESPACE --create-namespace

# Install phoenix app
helm upgrade $RELEASE_APP ./helm/phoenix-app --install \
--set environment.SECRET_KEY_BASE="Xu8gcFbCdq5N41GuWFIhbeiKcoFk0agnPmse4JKycYKADhCBHxmXXCCLnHdUSUZn" \
--set environment.DATABASE_URL="postgresql://$DB_USER:$DB_PASSWORD@$DB_HOST:$DB_PORT/$DB_NAME" \
--set ingress.enabled="true" \
--set ingress.host="localhost" \
--set image.repository="$IMAGE" \
--set image.tag="$TAG" \
--set image.pullPolicy="Never" \
--set labels.required.version="$TAG" \
--set ecto.migrate="true" \
-n $NAMESPACE --create-namespace
