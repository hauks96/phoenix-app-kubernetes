#!/bin/bash

# This script takes two commands as arguments: a success condition command and a failure condition command.
# It runs both commands in the background, waits for either of them to complete, and handles the results accordingly.
#
# Usage: ./kubectl_wait.sh "success_condition_command" "failure_condition_command"
#
# Example: ./kubectl_wait.sh \
#	"kubectl wait --for=condition=complete job/myjob --timeout=10s" \
#	"kubectl wait --for=condition=failure job/myjob --timeout=10s"

# Make sure that set -e is not set (terminate on command failures)
# We need this so that kill $pid wont kill parent process if pid is not found

if [ $# -ne 2 ]; then
    echo "Usage: $0 \"success_condition_command\" \"failure_condition_command\""
    exit 1
fi

success_command=$1
failure_command=$2

# Execute the wait command in the background
execute_success_cmd() {
    $success_command >/dev/null 2>&1
}

# Execute the failure check command in the background
execute_failure_cmd() {
    $failure_command >/dev/null 2>&1
}

# Terminate a process if it's running
terminate_process() {
    local pid=$1
    echo "Terminating process with PID $pid"
    kill $pid >/dev/null 2>&1 || true
}

# Run the wait command in the background
execute_success_cmd &
success_pid=$!

# Run the failure check command in the background
execute_failure_cmd &
failure_pid=$!

# Wait for either command to complete
wait -n $success_pid $failure_pid

# If one of the processes has exited with exit 0 (ok)
if [ $? -eq 0 ]; then
    # If the failure process is still running, success must have finished
    if ps -p $failure_pid > /dev/null; then
        echo "SUCCESS CONDITION MET. Terminating failure check."
        terminate_process $failure_pid
        exit 0;
    # If the failure process is not running, it must have finished
    else
        echo "FAILURE CONDITION MET. Terminating success check." >&2
        terminate_process $success_pid
        exit 1;
    fi
# One of the processes exited with non-zero exit code
else
    echo "Timeout reached." >&2
    
    if ps -p $failure_pid > /dev/null; then
        echo "Failure condition still running... Terminating"
        terminate_process $failure_pid
    else
        echo "Success condition still running... Terminating"
        terminate_process $success_pid
    fi

    exit 1;
fi

