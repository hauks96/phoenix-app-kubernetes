# When you compose up and close out you can start containers again like this
docker start smtp-server
docker start phoenix-database
docker start phoenix-app
docker logs --follow phoenix-app --tail 10